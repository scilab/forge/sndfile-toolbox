Read & write sound files format wrapper on multiplatform C library sndfile 
see http://www.mega-nerd.com/libsndfile/

supported format (read & write):
* Ogg Vorbis (Xiph Foundation)
* FLAC 16 bit
* AIFF (Apple/SGI 16 bit PCM)
* AIFF (Apple/SGI 32 bit float)
* AIFF (Apple/SGI 8 bit PCM)
* AU (Sun/Next 16 bit PCM)
* AU (Sun/Next 8-bit u-law)
* CAF (Apple 16 bit PCM)
* OKI Dialogic VOX ADPCM
* WAV (Microsoft 16 bit PCM)
* WAV (Microsoft 32 bit float)
* WAV (Microsoft 4 bit IMA ADPCM)
* WAV (Microsoft 4 bit MS ADPCM)
* WAV (Microsoft 8 bit PCM)

Allan CORNET - 2010

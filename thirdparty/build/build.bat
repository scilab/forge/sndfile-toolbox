::  Copyright (C) 2024 - UTC - Stéphane MOTTELET
::  This program is free software; you can redistribute it and/or
::  modify it under the terms of the GNU General Public
::  License as published by the Free Software Foundation; either
::  version 2.1 of the License, or (at your option) any later version.
::
::  libsndfile build script for Linux and macOS
::  cmake should be in the PATH

set LIBSNDFILE_VER=1.2.2
cd ..
set THIRDPARTY=%cd%
set INSTALL_PREFIX=%THIRDPARTY%\Windows\%PROCESSOR_ARCHITECTURE%
cd build
curl -LO https://github.com/libsndfile/libsndfile/releases/download/%LIBSNDFILE_VER%/libsndfile-%LIBSNDFILE_VER%-win64.zip
tar -xf libsndfile-%LIBSNDFILE_VER%-win64.zip
cd libsndfile-%LIBSNDFILE_VER%-win64
robocopy lib %INSTALL_PREFIX%\lib /e
mkdir %INSTALL_PREFIX%\bin
copy bin\sndfile.dll %INSTALL_PREFIX%\bin
copy lib\sndfile.lib %INSTALL_PREFIX%\bin
robocopy include %INSTALL_PREFIX%\include /e
cd ..



#!/bin/sh
# Copyright (C) 2024 - UTC - Stéphane MOTTELET
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# libsndfile build script for Linux and macOS
# cmake should be in the PATH

OS=$(uname -s)
THIRDPARTY="$(cd ..;pwd)/$(uname -s)/$(uname -m)"
cd build
LIBSNDFILE_VER=1.2.2

curl -LO https://github.com/libsndfile/libsndfile/releases/download/${LIBSNDFILE_VER}/libsndfile-${LIBSNDFILE_VER}.tar.xz
tar xf libsndfile-${LIBSNDFILE_VER}.tar.xz
cd libsndfile-${LIBSNDFILE_VER}
mkdir -p build
cd build
cmake -DCMAKE_INSTALL_PREFIX="${THIRDPARTY}" \
-DBUILD_SHARED_LIBS:BOOL=ON \
-DCMAKE_OSX_SYSROOT=$SDKROOT \
-DBUILD_TESTS:BOOL=OFF \
-DBUILD_PROGRAMS:BOOL=OFF \
-DBUILD_TESTING:BOOL=OFF \
-DINSTALL_MANPAGES:BOOL=OFF \
-DINSTALL_PKGCONFIG_MODULE:BOOL=OFF \
..
cmake --build . --target install  --parallel --config Release

if [ "$OS" = "Darwin" ]; then
    deps=$(otool -L ${THIRDPARTY}/lib/libsndfile.dylib | grep -e @rpath | grep -v libsndfile | awk -F '[/ ]' '{print $2}')
    for dep in $deps; do
        lib=$(echo $dep | awk -F '.' '{print $1}').dylib
        cp ${CONDA_PREFIX}/lib/$dep ${THIRDPARTY}/lib/$dep
        ln -sf $dep ${THIRDPARTY}/lib/$lib
    done
elif [ "$OS" = "Linux" ]; then
    deps=$(ldd  ${THIRDPARTY}/lib/libsndfile.so | grep "/usr/lib" | awk -F ' ' '{print $1}')
    for dep in $deps; do
        lib=$(echo $dep | awk -F '.' '{print $1}').so
        cp /usr/lib/x86_64-linux-gnu/$dep ${THIRDPARTY}/lib/$dep
        ln -sf $dep ${THIRDPARTY}/lib/$lib
    done
fi

changelog of the sndfile_toolbox

sndfile_toolbox (0.5) (09/12/2024)
    * scilab 2025 build
    * thirdparty build/download
    * update builder (multi-platform).

sndfile_toolbox (0.4) (24/09/10)
    * add sfgetstring and sfsetstring to manage metadata in sound files. 
    * update builder (multi-platform).

sndfile_toolbox (0.3) (05/09/10)
    * update to support libsndfile-1.0.21
    * bug 35 fixed - sfwrite() did not work with multi-channels source.
    * add a new demo with a ogg file (multi-channels).
    * add sfgettlbxpath() macro returns sndfile toolbox root path.
    * extends sfsupportedformat returns also compressed format names.

sndfile_toolbox (0.2) (12/14/09)
    * add some demos (read, write sound files)

sndfile_toolbox (0.1)
    * first version 
    
 -- Author <allan.cornet@scilab.org>  09/30/09


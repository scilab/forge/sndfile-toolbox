// ====================================================================
// Copyright DIGITEO 2010 - Allan CORNET
// ====================================================================
#include <stdlib.h>
#include <sndfile.h>
#include "api_scilab.h"
#include "Scierror.h"
#include "localization.h"
/* ==================================================================== */
#ifdef _MSC_VER
#define strdup _strdup
#endif
/* ==================================================================== */
static char* getFormatFile(int format);
/* ==================================================================== */
int sci_sfsupportedformat(char *fname, void* pvApiCtx)
{
	SciErr strErr;

	char **output_fullname = NULL;
	char **output_compressedname = NULL;
	int m_out = 0;
	int n_out = 0;

	int count = 0;

	CheckLhs(1,2); 
	CheckRhs(0,0); 

	sf_command (NULL, SFC_GET_SIMPLE_FORMAT_COUNT, &count, sizeof (int)) ;

	m_out = count;
	n_out = 1;

	output_fullname = (char**)malloc(sizeof(char*) * (m_out * n_out));
	if (Lhs == 2)
	{
		output_compressedname = (char**)malloc(sizeof(char*) * (m_out * n_out));

		if (output_compressedname == NULL)
		{
			int k = 0;
			for (k = 0 ; k < count ; k++)
			{
				if (output_fullname[k]) {free(output_fullname[k]); output_fullname[k] = NULL;}
			}
			free(output_fullname); output_fullname = NULL;

			Scierror(999, _("%s: No more memory.\n"), fname);
			return 0;
		}
	}

	if (output_fullname)
	{
		int k = 0;
		for (k = 0 ; k < count ; k++)
		{
			SF_FORMAT_INFO  format_info ;
			format_info.format = k ;
			sf_command (NULL, SFC_GET_SIMPLE_FORMAT, &format_info, sizeof (format_info)) ;
			output_fullname[k] = strdup(format_info.name);
			if (Lhs == 2)
			{
				output_compressedname[k] = getFormatFile(format_info.format);
			}
		}

		strErr = createMatrixOfString(pvApiCtx, Rhs + 1, m_out, n_out, output_fullname);
		LhsVar(1) = Rhs + 1; 

		if (Lhs == 2)
		{
			strErr = createMatrixOfString(pvApiCtx, Rhs + 2, m_out, n_out, output_compressedname);
			LhsVar(2) = Rhs + 2; 
		}

		for (k = 0 ; k < count ; k++)
		{
			if (output_fullname[k]) {free(output_fullname[k]); output_fullname[k] = NULL;}
			if (Lhs == 2)
			{
				if (output_compressedname[k]) {free(output_compressedname[k]); output_compressedname[k] = NULL;}
			}
		}
		free(output_fullname); output_fullname = NULL;

		if (Lhs == 2)
		{
			free(output_compressedname); output_compressedname = NULL;
		}

		PutLhsVar();
	}
	else
	{
		Scierror(999, _("%s: No more memory.\n"), fname);
	}
	return 0;
}
/* ==================================================================== */
static char* getFormatFile(int format)
{
	char *major = NULL;
	char *minor = NULL;
	char *fmt = NULL;

	switch (format & SF_FORMAT_TYPEMASK)
	{
	case SF_FORMAT_WAV : major = strdup("wav");break; /* Microsoft WAV format (little endian default). */
	case SF_FORMAT_AIFF : major = strdup("aiff");break; /* Apple/SGI AIFF format (big endian). */
	case SF_FORMAT_AU : major = strdup("au");break; /* Sun/NeXT AU format (big endian). */
	case SF_FORMAT_RAW : major = strdup("raw");break; /* RAW PCM data. */
	case SF_FORMAT_PAF : major = strdup("paf");break; /* Ensoniq PARIS file format. */
	case SF_FORMAT_SVX : major = strdup("svx");break; /* Amiga IFF / SVX8 / SV16 format. */
	case SF_FORMAT_NIST : major = strdup("nist");break; /* Sphere NIST format. */
	case SF_FORMAT_VOC : major = strdup("voc");break; /* VOC files. */
	case SF_FORMAT_IRCAM : major = strdup("ircam");break; /* Berkeley/IRCAM/CARL */
	case SF_FORMAT_W64 : major = strdup("w64");break; /* Sonic Foundry's 64 bit RIFF/WAV */
	case SF_FORMAT_MAT4 : major = strdup("mat4");break; /* Matlab (tm) V4.2 / GNU Octave 2.0 */
	case SF_FORMAT_MAT5 : major = strdup("mat5");break; /* Matlab (tm) V5.0 / GNU Octave 2.1 */
	case SF_FORMAT_PVF : major = strdup("pvf");break; /* Portable Voice Format */
	case SF_FORMAT_XI : major = strdup("xi");break; /* Fasttracker 2 Extended Instrument */
	case SF_FORMAT_HTK : major = strdup("htk");break; /* HMM Tool Kit format */
	case SF_FORMAT_SDS : major = strdup("sds");break; 	/* Midi Sample Dump Standard */
	case SF_FORMAT_AVR : major = strdup("avr");break; /* Audio Visual Research */
	case SF_FORMAT_WAVEX : major = strdup("wavx");break; 		/* MS WAVE with WAVEFORMATEX */
	case SF_FORMAT_SD2 : major = strdup("sd2");break; /* Sound Designer 2 */
	case SF_FORMAT_FLAC : major = strdup("flac");break; /* FLAC lossless file format */
	case SF_FORMAT_CAF : major = strdup("caf");break; /* Core Audio File format */
	case SF_FORMAT_WVE : major = strdup("wfe");break; /* Psion WVE format */
	case SF_FORMAT_OGG : major = strdup("ogg");break; /* Xiph OGG container */
	case SF_FORMAT_MPC2K : major = strdup("mpc2k");break; 		/* Akai MPC 2000 sampler */
	case SF_FORMAT_RF64 : major = strdup("rf64");break; /* RF64 WAV file */			

	default : major = strdup("unknown");break ;
	} ;

	switch (format & SF_FORMAT_SUBMASK)
	{
	case SF_FORMAT_PCM_S8 : minor = strdup("int8");break ; /* Signed 8 bit data */
	case SF_FORMAT_PCM_16 : minor = strdup("int16");break ; /* Signed 16 bit data */
	case SF_FORMAT_PCM_24 : minor = strdup("int24");break ; /* Signed 24 bit data */
	case SF_FORMAT_PCM_32 : minor = strdup("int32");break ; /* Signed 32 bit data */
	case SF_FORMAT_PCM_U8 : minor = strdup("uint8");break ; /* Unsigned 8 bit data (WAV and RAW only) */
	case SF_FORMAT_FLOAT : minor = strdup("float");break ; /* 32 bit float data */
	case SF_FORMAT_DOUBLE : minor = strdup("double");break ; /* 64 bit float data */
	case SF_FORMAT_ULAW : minor = strdup("ulaw");break ; /* U-Law encoded. */
	case SF_FORMAT_ALAW : minor = strdup("alaw");break ; /* A-Law encoded. */
	case SF_FORMAT_IMA_ADPCM : minor = strdup("ima_adpcm");break ; /* IMA ADPCM. */
	case SF_FORMAT_MS_ADPCM : minor = strdup("ms_adpcm");break ; /* Microsoft ADPCM. */
	case SF_FORMAT_GSM610 : minor = strdup("gsm610");break ; /* GSM 6.10 encoding. */
	case SF_FORMAT_VOX_ADPCM : minor = strdup("vox_adpcm");break ; /* OKI / Dialogix ADPCM */
	case SF_FORMAT_G721_32 : minor = strdup("g721_32");break ; /* 32kbs G721 ADPCM encoding. */
	case SF_FORMAT_G723_24 : minor = strdup("g723_24");break ; /* 24kbs G723 ADPCM encoding. */
	case SF_FORMAT_G723_40 : minor = strdup("g723_40");break ; /* 40kbs G723 ADPCM encoding. */
	case SF_FORMAT_DWVW_12 : minor = strdup("dwvw_12");break ; /* 12 bit Delta Width Variable Word encoding. */
	case SF_FORMAT_DWVW_16 : minor = strdup("dwvw_16");break ; /* 16 bit Delta Width Variable Word encoding. */
	case SF_FORMAT_DWVW_24 : minor = strdup("dwvw_24");break ; /* 24 bit Delta Width Variable Word encoding. */
	case SF_FORMAT_DWVW_N : minor = strdup("dwvw_n");break ; /* N bit Delta Width Variable Word encoding. */
	case SF_FORMAT_DPCM_8 : minor = strdup("dpcm_8");break ; /* 8 bit differential PCM (XI only) */
	case SF_FORMAT_DPCM_16 : minor = strdup("dpcm_16");break ; /* 16 bit differential PCM (XI only) */
	case SF_FORMAT_VORBIS : minor = strdup("vorbis");break ; /* Xiph Vorbis encoding. */
	default : minor = strdup("unknown");break ;
	} ;

	if (major && minor)
	{
		fmt = (char*)malloc(sizeof(char) * ((int) strlen(major) + (int) strlen(minor) + (int) strlen("-") + 1));
		if (fmt)
		{
			sprintf(fmt,"%s-%s",major,minor);
		}
		free(major); major = NULL;
		free(minor); minor = NULL;
	}
	return fmt;
}
/* ==================================================================== */


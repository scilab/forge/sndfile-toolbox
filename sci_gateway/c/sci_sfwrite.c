// ====================================================================
// Copyright DIGITEO 2009 - Allan CORNET
// ====================================================================
#include <stdlib.h>
#include <sndfile.h>
#include "api_scilab.h"
#include "Scierror.h"
#include "localization.h"
/* ==================================================================== */
#ifdef _MSC_VER
#define strdup _strdup
#endif
/* ==================================================================== */
#define	BUFFER_FRAMES 8192
#define DEFAULT_SAMPLERATE 44100.0
#define DEFAULT_FORMAT "wav-int16"
/* ==================================================================== */
static int strtofmtminor(char *minor);
static int strtofmtmajor(char *major);
static int strtofmt(char *fmt);
/* ==================================================================== */
int sci_sfwrite(char *fname, void* pvApiCtx)
{
	SciErr strErr;

	int *piAddressVarOne = NULL;
	int m1 = 0, n1 = 0;
	char *pStVarOne = NULL;
	int lenStVarOne = 0;
	int iType1 = 0;

	int *piAddressVarTwo = NULL;
	int m2 = 0, n2 = 0;
	int rows = 0, cols = 0;
	double *pdVarTwo = NULL;
	int iType2 = 0;

	char *fmt = strdup(DEFAULT_FORMAT);

	double samplerate = DEFAULT_SAMPLERATE;

	SNDFILE * file = NULL;
	SF_INFO sfinfo ;

	CheckLhs(0,0); 
	CheckRhs(2,4); 

	if (Rhs > 3)
	{
		int *piAddressVarFour = NULL;
		int m4 = 0, n4 = 0;
		char *pStVarFour = NULL;
		int lenStVarFour = 0;
		int iType4 = 0;

		strErr = getVarAddressFromPosition(pvApiCtx, 4, &piAddressVarFour);
		if(strErr.iErr)
		{
			printError(&strErr, 0);
			return 0;
		}	

		strErr = getVarType(pvApiCtx, piAddressVarFour, &iType4);
		if ( iType4 != sci_strings )
		{
			Scierror(999,_("%s: Wrong type for input argument #%d: A string expected.\n"),fname,4);
			return 0;
		}

		strErr = getMatrixOfString(pvApiCtx, piAddressVarFour,&m4, &n4, &lenStVarFour, NULL);
		if(strErr.iErr)
		{
			printError(&strErr, 0);
			return 0;
		}

		if ( (m4 != n4) && (n4 != 1) ) 
		{
			Scierror(999,_("%s: Wrong size for input argument #%d: A string expected.\n"),fname,4);
			return 0;
		}

		pStVarFour = (char*)malloc(sizeof(char)*(lenStVarFour + 1));
		if (pStVarFour)
		{
			strErr = getMatrixOfString(pvApiCtx, piAddressVarFour, &m4, &n4, &lenStVarFour, &pStVarFour);
			if(strErr.iErr)
			{
				printError(&strErr, 0);
				return 0;
			}
			if (fmt) {free(fmt); fmt = NULL;}
			fmt = pStVarFour;
		}
		else
		{
			Scierror(999,_("%s: No more memory.\n"),fname);
			return 0;
		}
	}

	if (Rhs > 2)
	{
		int *piAddressVarThree = NULL;
		int m3 = 0, n3 = 0;
		double *pdVarThree = NULL;
		int iType3 = 0;

		strErr = getVarAddressFromPosition(pvApiCtx, 3, &piAddressVarThree);
		if(strErr.iErr)
		{
			printError(&strErr, 0);
			return 0;
		}

		strErr = getVarType(pvApiCtx, piAddressVarThree, &iType3);
		if ( iType3 != sci_matrix )
		{
			Scierror(999,_("%s: Wrong type for input argument #%d: A scalar expected.\n"),fname,3);
			return 0;
		}

		strErr = getMatrixOfDouble(pvApiCtx, piAddressVarThree,&m2,&n2,&pdVarThree);
		if(strErr.iErr)
		{
			printError(&strErr, 0);
			return 0;
		}	
		if ( (m3 != n3) && (n3 != 1) ) 
		{
			Scierror(999,_("%s: Wrong size for input argument #%d: A scalar expected.\n"),fname,3);
			return 0;
		}
		samplerate = pdVarThree[0];
	}

	strErr = getVarAddressFromPosition(pvApiCtx, 2, &piAddressVarTwo);
	if(strErr.iErr)
	{
		printError(&strErr, 0);
		return 0;
	}	

	strErr = getVarType(pvApiCtx, piAddressVarTwo, &iType2);
	if(strErr.iErr)
	{
		printError(&strErr, 0);
		return 0;
	}	
	if ( iType2 != sci_matrix )
	{
		if (pStVarOne) {free(pStVarOne); pStVarOne = NULL;}
		Scierror(999,_("%s: Wrong type for input argument #%d: A scalar expected.\n"),fname,2);
		return 0;
	}

	strErr = getMatrixOfDouble(pvApiCtx, piAddressVarTwo,&m2,&n2,&pdVarTwo);
	if(strErr.iErr)
	{
		printError(&strErr, 0);
		return 0;
	}	

	strErr = getVarAddressFromPosition(pvApiCtx, 1, &piAddressVarOne);
	if(strErr.iErr)
	{
		printError(&strErr, 0);
		return 0;
	}	

	strErr = getVarType(pvApiCtx, piAddressVarOne, &iType1);
	if(strErr.iErr)
	{
		printError(&strErr, 0);
		return 0;
	}

	if ( iType1 != sci_strings )
	{
		Scierror(999,_("%s: Wrong type for input argument #%d: A string expected.\n"),fname,1);
		return 0;
	}

	strErr = getMatrixOfString(pvApiCtx, piAddressVarOne,&m1, &n1, &lenStVarOne, &pStVarOne);
	if(strErr.iErr)
	{
		printError(&strErr, 0);
		return 0;
	}		
	if ( (m1 != n1) && (n1 != 1) ) 
	{
		Scierror(999,_("%s: Wrong size for input argument #%d: A string expected.\n"),fname,1);
		return 0;
	}

	pStVarOne = (char*)malloc(sizeof(char)*(lenStVarOne + 1));
	if (pStVarOne)
	{
		strErr = getMatrixOfString(pvApiCtx, piAddressVarOne, &m1, &n1, &lenStVarOne, &pStVarOne);
		if(strErr.iErr)
		{
			printError(&strErr, 0);
			return 0;
		}
	}
	else
	{
		Scierror(999,_("%s: No more memory.\n"),fname);
		return 0;
	}

	memset (&sfinfo, 0, sizeof (sfinfo));

	sfinfo.format = strtofmt(fmt);
	if (sfinfo.format == 0)
	{
		if (pStVarOne) { free(pStVarOne); pStVarOne = NULL;}
		Scierror(999,_("%s: Wrong value for input argument #%d: Bad format %s.\n"),fname,4,fmt);
		return 0;
	} 

	sfinfo.samplerate = (int)samplerate;
	if (sfinfo.samplerate < 1)
	{
		if (pStVarOne) { free(pStVarOne); pStVarOne = NULL;}
		Scierror(999,_("%s: Wrong value for input argument #%d: Bad sample rate : %d.\n"),fname,3,sfinfo.samplerate);
		return 0;
	}

	rows = m2;
	cols = n2;

	if (cols > rows)
	{
		if (pStVarOne) { free(pStVarOne); pStVarOne = NULL;}
		Scierror(999,_("%s: Wrong value for input argument #%d:\nAudio data should have one column per channel, but supplied data has %d rows and %d columns.\n"),fname,2,rows, cols);
		return 0;
	}

	sfinfo.channels = cols ;
	if ((file = sf_open (pStVarOne, SFM_WRITE, &sfinfo)) == NULL)
	{
		Scierror(999,_("%s: Couldn't open file %s : %s"), fname, pStVarOne, sf_strerror (NULL));
		if (pStVarOne) { free(pStVarOne); pStVarOne = NULL;}
		return 0;
	}
	else
	{
		double *buffer = (double*)malloc(sizeof(double)* (BUFFER_FRAMES * sfinfo.channels));
		int writecount = 0;
		long total = 0;
		int l = 0;

		if (pStVarOne) { free(pStVarOne); pStVarOne = NULL;}

		if (buffer == NULL)
		{
			Scierror(999,_("%s: No more memory.\n"),fname);
			return 0;
		}

		sf_command (file, SFC_SET_NORM_DOUBLE, NULL, SF_TRUE) ;

		l = 0;
		do
		{
			int ch = 0;
			writecount = BUFFER_FRAMES ;

			/* Make sure we don't read more frames than we allocated. */
			if (total + writecount > rows) writecount = rows - total ;

			for (ch = 0 ; ch < sfinfo.channels ; ch++)
			{	
				int k = 0;
				for (k = 0 ; k < writecount ; k++)
				{
					buffer[k * sfinfo.channels + ch] = pdVarTwo[l++] ;
				}
			}

			if (writecount > 0) sf_writef_double (file, buffer, writecount);

			total += writecount ;
		} while (writecount > 0 && total < rows) ;

		if (buffer) {free(buffer); buffer = NULL;}
		sf_close (file) ;
	}

	LhsVar(1) = 0; 
	PutLhsVar();

	return 0;
}
/* ==================================================================== */
int strtofmtminor(char *minor)
{
	if (minor)
	{
		if (strcmp(minor,"int8")== 0) return  SF_FORMAT_PCM_S8; /* Signed 8 bit data */
		if (strcmp(minor,"int16")== 0) return SF_FORMAT_PCM_16; /* Signed 16 bit data */
		if (strcmp(minor,"int24")== 0) return SF_FORMAT_PCM_24; /* Signed 24 bit data */
		if (strcmp(minor,"int32")== 0) return SF_FORMAT_PCM_32; /* Signed 32 bit data */
		if (strcmp(minor,"uint8")== 0) return SF_FORMAT_PCM_U8; /* Unsigned 8 bit data (WAV and RAW only) */
		if (strcmp(minor,"float")== 0) return SF_FORMAT_FLOAT; /* 32 bit float data */
		if (strcmp(minor,"double")== 0) return  SF_FORMAT_DOUBLE; /* 64 bit float data */
		if (strcmp(minor,"ulaw")== 0) return  SF_FORMAT_ULAW; /* U-Law encoded. */
		if (strcmp(minor,"alaw")== 0) return SF_FORMAT_ALAW; /* A-Law encoded. */
		if (strcmp(minor,"ima_adpcm")== 0) return SF_FORMAT_IMA_ADPCM; /* IMA ADPCM. */
		if (strcmp(minor,"ms_adpcm")== 0) return SF_FORMAT_MS_ADPCM; /* Microsoft ADPCM. */
		if (strcmp(minor,"gsm610")== 0) return SF_FORMAT_GSM610; /* GSM 6.10 encoding. */
		if (strcmp(minor,"vox_adpcm")== 0) return  SF_FORMAT_VOX_ADPCM; /* OKI / Dialogix ADPCM */
		if (strcmp(minor,"g721_32")== 0) return SF_FORMAT_G721_32; /* 32kbs G721 ADPCM encoding. */
		if (strcmp(minor,"g723_24")== 0) return SF_FORMAT_G723_24; /* 24kbs G723 ADPCM encoding. */
		if (strcmp(minor,"g723_40")== 0) return SF_FORMAT_G723_40; /* 40kbs G723 ADPCM encoding. */
		if (strcmp(minor,"dwvw_12")== 0) return SF_FORMAT_DWVW_12; /* 12 bit Delta Width Variable Word encoding. */
		if (strcmp(minor,"dwvw_16")== 0) return SF_FORMAT_DWVW_16; /* 16 bit Delta Width Variable Word encoding. */
		if (strcmp(minor,"dwvw_24")== 0) return SF_FORMAT_DWVW_24; /* 24 bit Delta Width Variable Word encoding. */
		if (strcmp(minor,"dwvw_n")== 0) return SF_FORMAT_DWVW_N; /* N bit Delta Width Variable Word encoding. */
		if (strcmp(minor,"dpcm_8")== 0) return SF_FORMAT_DPCM_8; /* 8 bit differential PCM (XI only) */
		if (strcmp(minor,"dpcm_16")== 0) return SF_FORMAT_DPCM_16; /* 16 bit differential PCM (XI only) */
		if (strcmp(minor,"vorbis")== 0) return SF_FORMAT_VORBIS; /* Xiph Vorbis encoding. */
	}
	return 0;
}
/* ==================================================================== */
int strtofmtmajor(char *major)
{
	if (major)
	{
		if (strcmp(major,"wav")== 0) return SF_FORMAT_WAV; /* Microsoft WAV format (little endian default). */
		if (strcmp(major,"aiff")== 0) return SF_FORMAT_AIFF; /* Apple/SGI AIFF format (big endian). */
		if (strcmp(major,"au")== 0) return SF_FORMAT_AU; /* Sun/NeXT AU format (big endian). */
		if (strcmp(major,"raw")== 0) return SF_FORMAT_RAW; /* RAW PCM data. */
		if (strcmp(major,"paf")== 0) return SF_FORMAT_PAF; /* Ensoniq PARIS file format. */
		if (strcmp(major,"svx")== 0) return SF_FORMAT_SVX; /* Amiga IFF / SVX8 / SV16 format. */
		if (strcmp(major,"nist")== 0) return SF_FORMAT_NIST; /* Sphere NIST format. */
		if (strcmp(major,"voc")== 0) return SF_FORMAT_VOC; /* VOC files. */
		if (strcmp(major,"ircam")== 0) return SF_FORMAT_IRCAM; /* Berkeley/IRCAM/CARL */
		if (strcmp(major,"w64")== 0) return SF_FORMAT_W64; /* Sonic Foundry's 64 bit RIFF/WAV */
		if (strcmp(major,"mat4")== 0) return SF_FORMAT_MAT4; /* Matlab (tm) V4.2 / GNU Octave 2.0 */
		if (strcmp(major,"mat5")== 0) return SF_FORMAT_MAT5; /* Matlab (tm) V5.0 / GNU Octave 2.1 */
		if (strcmp(major,"pvf")== 0) return SF_FORMAT_PVF; /* Portable Voice Format */
		if (strcmp(major,"xi")== 0) return SF_FORMAT_XI; /* Fasttracker 2 Extended Instrument */
		if (strcmp(major,"htk")== 0) return SF_FORMAT_HTK; /* HMM Tool Kit format */
		if (strcmp(major,"sds")== 0) return SF_FORMAT_SDS; /* Midi Sample Dump Standard */
		if (strcmp(major,"avr")== 0) return SF_FORMAT_AVR; /* Audio Visual Research */
		if (strcmp(major,"wavx")== 0) return SF_FORMAT_WAVEX; /* MS WAVE with WAVEFORMATEX */
		if (strcmp(major,"sd2")== 0) return SF_FORMAT_SD2; /* Sound Designer 2 */
		if (strcmp(major,"flac")== 0) return SF_FORMAT_FLAC; /* FLAC lossless file format */
		if (strcmp(major,"caf")== 0) return SF_FORMAT_CAF; /* Core Audio File format */
		if (strcmp(major,"wfe")== 0) return SF_FORMAT_WVE; /* Psion WVE format */
		if (strcmp(major,"ogg")== 0) return SF_FORMAT_OGG; /* Xiph OGG container */
		if (strcmp(major,"mpc2k")== 0) return SF_FORMAT_MPC2K;	/* Akai MPC 2000 sampler */
		if (strcmp(major,"rf64")== 0) return SF_FORMAT_RF64; /* RF64 WAV file */
	}
	return 0;
}
/* ==================================================================== */
int strtofmt(char *fmt)
{
	int minor = 0;
	int major = 0;
	if (fmt)
	{
		char *majorstr = NULL;
		char *minorstr = NULL;

		minorstr = strchr(fmt,'-');
		if (minorstr)
		{
			if ((int)strlen(minorstr) > 1) 
			{
				minorstr = strdup(&minorstr[1]);
				majorstr = strdup(fmt);
				if ( (strlen(majorstr) - strlen(minorstr) - 1) > 0)
				{
					majorstr[strlen(majorstr) - strlen(minorstr) - 1] = '\0';
				}
				else
				{
					free(majorstr);
					majorstr = NULL;
				}

				major = strtofmtmajor(majorstr);
				minor = strtofmtminor(minorstr);

				if (minorstr) {free(minorstr); minorstr = NULL;}
				if (majorstr) {free(majorstr); majorstr = NULL;}
			}
		}
		if (major && minor) return (major | minor);
	}
	return 0;
}
/* ==================================================================== */

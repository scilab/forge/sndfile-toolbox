// ====================================================================
// Copyright DIGITEO 2010 - Allan CORNET
// ====================================================================
#include <stdlib.h>
#include <sndfile.h>
#include "api_scilab.h"
#include "Scierror.h"
#include "localization.h"
#include "BOOL.h"
// ====================================================================
int sci_sfsetbroadcastinfo(char *fname, void* pvApiCtx)
{
	LhsVar(1) = 0;
	PutLhsVar();
	return 0;
}
// ====================================================================

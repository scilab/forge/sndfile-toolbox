#ifndef __SNDFILE_C_GW_HXX__
#define __SNDFILE_C_GW_HXX__

#ifdef _MSC_VER
#ifdef SNDFILE_C_GW_EXPORTS
#define SNDFILE_C_GW_IMPEXP __declspec(dllexport)
#else
#define SNDFILE_C_GW_IMPEXP __declspec(dllimport)
#endif
#else
#define SNDFILE_C_GW_IMPEXP
#endif

extern "C" SNDFILE_C_GW_IMPEXP int sndfile_c(wchar_t* _pwstFuncName);



#endif /* __SNDFILE_C_GW_HXX__ */

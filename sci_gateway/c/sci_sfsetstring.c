// ====================================================================
// Copyright DIGITEO 2010 - Allan CORNET
// ====================================================================
#include <stdlib.h>
#include <sndfile.h>
#include "api_scilab.h"
#include "Scierror.h"
#include "localization.h"
#include "sci_malloc.h"
#include "BOOL.h"
/* ==================================================================== */
#define STR_TITLE       "title"
#define STR_COPYRIGHT   "copyright"
#define STR_SOFTWARE    "software"
#define STR_ARTIST      "artist"
#define STR_COMMENT     "comment"
#define STR_DATE        "date"
#define STR_ALBUM       "album"
#define STR_LICENSE     "license"
/* ==================================================================== */
static int strTypeToInt(char *strType);
static BOOL isTypeSupported(char *strType);
static char *getStringFromRhs(char *fname, void* pvApiCtx, int RhsPosition, int *iError);
/* ==================================================================== */
int sci_sfsetstring(char *fname, void* pvApiCtx)
{
	char *filename = NULL;
	char *strType = NULL;
	char *strComment = NULL;

	int iError = 0;

	SNDFILE * file = NULL;
	SF_INFO sfinfo;

	CheckRhs(3, 3);
	CheckLhs(0, 1);

	filename = getStringFromRhs(fname, pvApiCtx, 1, &iError);
	if (iError) return iError;

	strType = getStringFromRhs(fname, pvApiCtx, 2, &iError);
	if (iError) 
	{
		if (filename)
		{
			freeAllocatedSingleString(filename);
			filename = NULL;
		}
		return iError;
	}

	if (!isTypeSupported(strType))
	{
		if (strType)
		{
			freeAllocatedSingleString(strType);
			strType = NULL;
		}

		if (filename)
		{
			freeAllocatedSingleString(filename);
			filename = NULL;
		}
		Scierror(999, _("%s: Wrong value for input argument #%d: A string expected.\n"), fname, 2);
		return 0;
	}

	strComment = getStringFromRhs(fname, pvApiCtx, 3, &iError);
	if (iError) 
	{
		if (strType)
		{
			freeAllocatedSingleString(strType);
			strType = NULL;
		}

		if (filename)
		{
			freeAllocatedSingleString(filename);
			filename = NULL;
		}
		return iError;
	}

	memset (&sfinfo, 0, sizeof (sfinfo)) ;
	sfinfo.format = 0;
	file = sf_open (filename, SFM_RDWR, &sfinfo);
	if (file == NULL)
	{
		Scierror(999,_("%s: could not open file %s : %s"), fname, filename, sf_strerror (NULL));

		freeAllocatedSingleString(filename);
		filename = NULL;

		freeAllocatedSingleString(strType);
		strType = NULL;

		freeAllocatedSingleString(strComment);
		strComment = NULL;

		return 0;
	}

	iError = sf_set_string (file, strTypeToInt(strType), strComment);
	sf_close (file);

	if (iError)
	{
		createScalarBoolean(pvApiCtx, Rhs + 1, FALSE);
	}
	else
	{
		createScalarBoolean(pvApiCtx, Rhs + 1, TRUE);
	}

	LhsVar(1) = Rhs + 1;
	PutLhsVar();

	freeAllocatedSingleString(filename);
	filename = NULL;

	freeAllocatedSingleString(strType);
	strType = NULL;

	freeAllocatedSingleString(strComment);
	strComment = NULL;

	return 0;
}
/* ==================================================================== */
static int strTypeToInt(char *strType)
{
	if (strType)
	{
		if (strcmp(strType, STR_TITLE) == 0) return SF_STR_TITLE;
		if (strcmp(strType, STR_COPYRIGHT) == 0) return SF_STR_COPYRIGHT;
		if (strcmp(strType, STR_SOFTWARE) == 0) return SF_STR_SOFTWARE;
		if (strcmp(strType, STR_ARTIST) == 0) return SF_STR_ARTIST;
		if (strcmp(strType, STR_COMMENT) == 0) return SF_STR_COMMENT;
		if (strcmp(strType, STR_DATE) == 0) return SF_STR_DATE;
		if (strcmp(strType, STR_ALBUM) == 0) return SF_STR_ALBUM;
		if (strcmp(strType, STR_LICENSE) == 0) return SF_STR_LICENSE;
	}
	return -1;
}
/* ==================================================================== */
static BOOL isTypeSupported(char *strType)
{
	if (strType)
	{
		if (strcmp(strType, STR_TITLE) == 0) return TRUE;
		if (strcmp(strType, STR_COPYRIGHT) == 0) return TRUE;
		if (strcmp(strType, STR_SOFTWARE) == 0) return TRUE;
		if (strcmp(strType, STR_ARTIST) == 0) return TRUE;
		if (strcmp(strType, STR_COMMENT) == 0) return TRUE;
		if (strcmp(strType, STR_DATE) == 0) return TRUE;
		if (strcmp(strType, STR_ALBUM) == 0) return TRUE;
		if (strcmp(strType, STR_LICENSE) == 0) return TRUE;
	}
	return FALSE;
}
/* ==================================================================== */
static char *getStringFromRhs(char *fname, void* pvApiCtx, int RhsPosition, int *iError)
{
	SciErr sciErr;

	int *piAddressVar = NULL;
	char *val = NULL;

	sciErr = getVarAddressFromPosition(pvApiCtx, RhsPosition, &piAddressVar);

	if(sciErr.iErr)
	{
		printError(&sciErr, 0);
		*iError = sciErr.iErr;
		return NULL;
	}

	if ( !isStringType(pvApiCtx, piAddressVar) )
	{
		Scierror(999, _("%s: Wrong type for input argument #%d: A string expected.\n"), fname, RhsPosition);
		*iError = -1;
		return NULL;
	}

	if ( getAllocatedSingleString(pvApiCtx, piAddressVar, &val) )
	{
		Scierror(999, _("%s: Wrong size for input argument #%d: A string expected.\n"), fname, RhsPosition);
		*iError = -1;
		return NULL;
	}

	*iError = 0;
	return val;
}
/* ==================================================================== */

// ====================================================================
// Copyright DIGITEO 2009 - Allan CORNET
// ====================================================================
#include <stdlib.h>
#include <sndfile.h>
#include "api_scilab.h"
#include "Scierror.h"
#include "localization.h"
/* ==================================================================== */
#ifdef _MSC_VER
#define strdup _strdup
#endif
/* ==================================================================== */
int sci_sfversion(char *fname, void* pvApiCtx)
{
	SciErr strErr;

	char **output = NULL;
	int m_out = 1;
	int n_out = 1;

	char buffer[256];

	CheckLhs(1,1); 
	CheckRhs(0,0); 

	sf_command (NULL, SFC_GET_LIB_VERSION, buffer, sizeof (buffer));

	output = (char**)malloc(sizeof(char*) * (m_out * n_out));
	if (output)
	{
		output[0] = strdup(buffer);
		strErr = createMatrixOfString(pvApiCtx, Rhs + 1, m_out, n_out, output);
		LhsVar(1) = Rhs + 1; 
		PutLhsVar();

		free(output[0]); output[0] = NULL;
		free(output); output = NULL;
	}
	else
	{
		Scierror(999, _("%s: No more memory.\n"), fname);
	}
	return 0;
}
/* ==================================================================== */

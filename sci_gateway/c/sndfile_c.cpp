#include <wchar.h>
#include "sndfile_c.hxx"
extern "C"
{
#include "sndfile_c.h"
#include "addfunction.h"
}

#define MODULE_NAME L"sndfile_c"

int sndfile_c(wchar_t* _pwstFuncName)
{
    if(wcscmp(_pwstFuncName, L"sfversion") == 0){ addCFunction(L"sfversion", &sci_sfversion, MODULE_NAME); }
    if(wcscmp(_pwstFuncName, L"sfread") == 0){ addCFunction(L"sfread", &sci_sfread, MODULE_NAME); }
    if(wcscmp(_pwstFuncName, L"sfgetstring") == 0){ addCFunction(L"sfgetstring", &sci_sfgetstring, MODULE_NAME); }
    if(wcscmp(_pwstFuncName, L"sfsetstring") == 0){ addCFunction(L"sfsetstring", &sci_sfsetstring, MODULE_NAME); }
    if(wcscmp(_pwstFuncName, L"sfsetbroadcastinfo") == 0){ addCFunction(L"sfsetbroadcastinfo", &sci_sfsetbroadcastinfo, MODULE_NAME); }
    if(wcscmp(_pwstFuncName, L"sfgetbroadcastinfo") == 0){ addCFunction(L"sfgetbroadcastinfo", &sci_sfgetbroadcastinfo, MODULE_NAME); }
    if(wcscmp(_pwstFuncName, L"sfwrite") == 0){ addCFunction(L"sfwrite", &sci_sfwrite, MODULE_NAME); }
    if(wcscmp(_pwstFuncName, L"sfsupportedformat") == 0){ addCFunction(L"sfsupportedformat", &sci_sfsupportedformat, MODULE_NAME); }

    return 1;
}

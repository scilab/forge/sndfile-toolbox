#ifndef __SNDFILE_C_GW_H__
#define __SNDFILE_C_GW_H__

#include "c_gateway_prototype.h"

C_GATEWAY_PROTOTYPE(sci_sfgetbroadcastinfo);
C_GATEWAY_PROTOTYPE(sci_sfgetstring);
C_GATEWAY_PROTOTYPE(sci_sfread);
C_GATEWAY_PROTOTYPE(sci_sfsetbroadcastinfo);
C_GATEWAY_PROTOTYPE(sci_sfsetstring);
C_GATEWAY_PROTOTYPE(sci_sfsupportedformat);
C_GATEWAY_PROTOTYPE(sci_sfversion);
C_GATEWAY_PROTOTYPE(sci_sfwrite);

#endif /* __SNDFILE_C_GW_H__ */

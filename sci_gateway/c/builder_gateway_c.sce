// ====================================================================
// Copyright DIGITEO 2009 - Allan CORNET
// Copyright DIGITEO 2024 - UTC - Stéphane MOTTELET
// ====================================================================

gw_path = get_absolute_file_path();
if  getos() == "Windows"
    ARCH = getenv("PROCESSOR_ARCHITECTURE");
else
    ARCH = unix_g("uname -m");
end

THIRDPARTY=fullpath(fullfile(gw_path,"..","..","thirdparty",getos(),ARCH));
LIBSNDFILE_INCLUDE = fullfile(THIRDPARTY,"include");
LIBSNDFILE_LIB = fullfile(THIRDPARTY,"lib");
inter_cflags = ilib_include_flag(LIBSNDFILE_INCLUDE);

WITHOUT_AUTO_PUTLHSVAR = %t;

functions_table = [
        "sfversion"               ,"sci_sfversion";
        "sfread"                  ,"sci_sfread";
        "sfgetstring"             ,"sci_sfgetstring";
        "sfsetstring"             ,"sci_sfsetstring";
        "sfsetbroadcastinfo"      ,"sci_sfsetbroadcastinfo";
        "sfgetbroadcastinfo"      ,"sci_sfgetbroadcastinfo";
        "sfwrite"                 ,"sci_sfwrite";
        "sfsupportedformat"       ,"sci_sfsupportedformat"];

functions_files = ["sci_sfversion.c", ..
                   "sci_sfread.c", ..
                   "sci_sfwrite.c", ..
                   "sci_sfgetstring.c", ..
                   "sci_sfsetstring.c", ..
                   "sci_sfgetbroadcastinfo.c", ..
                   "sci_sfsetbroadcastinfo.c", ..
                   "sci_sfsupportedformat.c"];


if getos() <> "Windows"
    libs = ["libogg"
    "libvorbisenc"
    "libFLAC"
    "libopus"
    "libmpg123"
    "libmp3lame"
    "libvorbis"
    "libsndfile";
    ];
    libs = fullfile("..","..","thirdparty",getos(),ARCH,"lib",libs);
else
    libs = "sndfile";
    libs = fullfile("..","..","thirdparty",getos(),ARCH,"bin",libs);
end

tbx_build_gateway("sndfile_c", functions_table, functions_files, ..
                  get_absolute_file_path("builder_gateway_c.sce"), ..
                  libs, "", inter_cflags);

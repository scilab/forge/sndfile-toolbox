// ====================================================================
// Copyright DIGITEO 2010 - Allan CORNET
// ====================================================================
#include <stdlib.h>
#include <sndfile.h>
#include "api_scilab.h"
#include "Scierror.h"
#include "localization.h"
/* ==================================================================== */
#define STR_TITLE       "title"
#define STR_COPYRIGHT   "copyright"
#define STR_SOFTWARE    "software"
#define STR_ARTIST      "artist"
#define STR_COMMENT     "comment"
#define STR_DATE        "date"
#define STR_ALBUM       "album"
#define STR_LICENSE     "license"
/* ==================================================================== */
#ifdef _MSC_VER
#define strdup _strdup
#endif
/* ==================================================================== */
static int strTypeToInt(char *strType);
static char * intTypeToStr(int iType);
/* ==================================================================== */
static char *getValueString(char *filename, int strType, int *iErrorFile);
static char **getLabelStrings(int *sizeReturned);
static char **getValueStrings(char *filename, int *sizeReturned);
static BOOL isEmptyValueStrings(char **values, int sizeValues);
static char **freeStrings(char ** strs, int nbStrs);
/* ==================================================================== */
static int sci_sfgetstring_one_rhs(char *fname, void* _pvCtx);
static int sci_sfgetstring_two_rhs(char *fname, void* _pvCtx);
/* ==================================================================== */
static char *getFilenameFromRhs(char *fname, void* _pvCtx);
static char *getParameterFromRhs(char *fname, void* _pvCtx);
/* ==================================================================== */
int sci_sfgetstring(char *fname, void* pvApiCtx)
{
	CheckRhs(1, 2);
	CheckLhs(1, 2);

	if (Rhs == 2)
	{
		return sci_sfgetstring_two_rhs(fname, pvApiCtx);
	}
	else
	{
		return sci_sfgetstring_one_rhs(fname, pvApiCtx);
	}
	return 0;
}
/* ==================================================================== */
static int strTypeToInt(char *strType)
{
	if (strType)
	{
		if (strcmp(strType, STR_TITLE) == 0) return SF_STR_TITLE;
		if (strcmp(strType, STR_COPYRIGHT) == 0) return SF_STR_COPYRIGHT;
		if (strcmp(strType, STR_SOFTWARE) == 0) return SF_STR_SOFTWARE;
		if (strcmp(strType, STR_ARTIST) == 0) return SF_STR_ARTIST;
		if (strcmp(strType, STR_COMMENT) == 0) return SF_STR_COMMENT;
		if (strcmp(strType, STR_DATE) == 0) return SF_STR_DATE;
		if (strcmp(strType, STR_ALBUM) == 0) return SF_STR_ALBUM;
		if (strcmp(strType, STR_LICENSE) == 0) return SF_STR_LICENSE;
	}
	return -1;
}
/* ==================================================================== */
static char * intTypeToStr(int iType)
{
	if (iType == SF_STR_TITLE) return strdup(STR_TITLE);
	if (iType == SF_STR_COPYRIGHT) return strdup(STR_COPYRIGHT);
	if (iType == SF_STR_SOFTWARE) return strdup(STR_SOFTWARE);
	if (iType == SF_STR_ARTIST) return strdup(STR_ARTIST);
	if (iType == SF_STR_COMMENT) return strdup(STR_COMMENT);
	if (iType == SF_STR_DATE) return strdup(STR_DATE);
	if (iType == SF_STR_ALBUM) return strdup(STR_ALBUM);
	if (iType == SF_STR_LICENSE) return strdup(STR_LICENSE);

	return strdup("");
}
/* ==================================================================== */
static int sci_sfgetstring_one_rhs(char *fname, void* pvApiCtx)
{
	int sizeValues = 0;
	char *filename = getFilenameFromRhs(fname, pvApiCtx);
	char **values = getValueStrings(filename, &sizeValues);

	if (sizeValues == -1)
	{
		Scierror(999,_("%s: could not open file %s : %s"), fname, filename, sf_strerror (NULL));

		values = freeStrings(values, sizeValues);

		if (filename)
		{
			freeAllocatedSingleString(filename);
			filename = NULL;
		}

		return 0;
	}

	if (filename)
	{
		freeAllocatedSingleString(filename);
		filename = NULL;
	}


	if (Lhs == 2)
	{
		if (isEmptyValueStrings(values, sizeValues))
		{
			createEmptyMatrix(pvApiCtx, Rhs + 1);
			LhsVar(1) = Rhs + 1;

			createEmptyMatrix(pvApiCtx, Rhs + 2);
			LhsVar(2) = Rhs + 2;

			PutLhsVar();

			values = freeStrings(values, sizeValues);
		}
		else
		{
			SciErr sciErr;

			int sizeArrayLabel = 0;
			char **labels = getLabelStrings(&sizeArrayLabel);
			int m_out = sizeArrayLabel;
			int n_out = 1;

			if (labels == NULL)
			{
				values = freeStrings(values, sizeValues);
				labels = freeStrings(values, sizeArrayLabel);
				
				Scierror(999, _("%s: No more memory.\n"), fname);
				return 0;
			}
			
			sciErr = createMatrixOfString(pvApiCtx, Rhs + 1, m_out, n_out, labels);
			if(sciErr.iErr)
			{
				values = freeStrings(values, sizeValues);
				labels = freeStrings(values, sizeArrayLabel);

				printError(&sciErr, 0);
				return 0;
			}
			LhsVar(1) = Rhs + 1;

			sciErr = createMatrixOfString(pvApiCtx, Rhs + 2, m_out, n_out, values);
			if(sciErr.iErr)
			{
				values = freeStrings(values, sizeValues);
				labels = freeStrings(values, sizeArrayLabel);

				printError(&sciErr, 0);
				return 0;
			}
			LhsVar(2) = Rhs + 2;

			values = freeStrings(values, sizeValues);
			labels = freeStrings(values, sizeArrayLabel);

			PutLhsVar();
		}
	}
	else
	{
		SciErr sciErr;

		int m_out = sizeValues;
		int n_out = 1;

		sciErr = createMatrixOfString(pvApiCtx, Rhs + 1, m_out, n_out, values);
		if(sciErr.iErr)
		{
			values = freeStrings(values, sizeValues);

			printError(&sciErr, 0);
			return 0;
		}
		LhsVar(1) = Rhs + 1;

		values = freeStrings(values, sizeValues);

		PutLhsVar();
	}

	return 0;
}
/* ==================================================================== */
static int sci_sfgetstring_two_rhs(char *fname, void* pvApiCtx)
{
	char *filename = getFilenameFromRhs(fname, pvApiCtx);
	char *strType = getParameterFromRhs(fname, pvApiCtx);
	int iErrorFile = 0;
	char *val = getValueString(filename, strTypeToInt(strType), &iErrorFile);

	if (iErrorFile == -1)
	{
		Scierror(999,_("%s: could not open file %s : %s"), fname, filename, sf_strerror (NULL));

		if (strType)
		{
			freeAllocatedSingleString(strType);
			strType = NULL;
		}

		if (filename)
		{
			freeAllocatedSingleString(filename);
			filename = NULL;
		}

		return 0;
	}

	if (Lhs == 2)
	{
		if (val == NULL)
		{
			createEmptyMatrix(pvApiCtx, Rhs + 1);
			LhsVar(1) = Rhs + 1;

			createEmptyMatrix(pvApiCtx, Rhs + 2);
			LhsVar(2) = Rhs + 2;
		}
		else
		{
			createSingleString(pvApiCtx, Rhs + 1, strType);
			LhsVar(1) = Rhs + 1;

			createSingleString(pvApiCtx, Rhs + 2, val);
			LhsVar(2) = Rhs + 2;
		}

		PutLhsVar();
	}
	else
	{
		if (val == NULL)
		{
			createEmptyMatrix(pvApiCtx, Rhs + 1);
		}
		else
		{
			createSingleString(pvApiCtx, Rhs + 1, val);
		}

		LhsVar(1) = Rhs + 1;
		PutLhsVar();
	}

	if (val)
	{
		free(val);
		val = NULL;
	}

	freeAllocatedSingleString(strType);
	strType = NULL;

	if (filename)
	{
		freeAllocatedSingleString(filename);
		filename = NULL;
	}

	return 0;
}
/* ==================================================================== */
static char *getFilenameFromRhs(char *fname, void* pvApiCtx)
{
	SciErr sciErr;

	int *piAddressVarOne = NULL;
	char *filename = NULL;

	sciErr = getVarAddressFromPosition(pvApiCtx, 1, &piAddressVarOne);
	if(sciErr.iErr)
	{
		printError(&sciErr, 0);
		return NULL;
	}

	if ( !isStringType(pvApiCtx, piAddressVarOne) )
	{
		Scierror(999, _("%s: Wrong type for input argument #%d: A string expected.\n"), fname, 1);
		return NULL;
	}

	if ( getAllocatedSingleString(pvApiCtx, piAddressVarOne, &filename) )
	{
		Scierror(999, _("%s: Wrong size for input argument #%d: A string expected.\n"), fname, 1);
		return NULL;
	}

	return filename;
}
/* ==================================================================== */
static char *getParameterFromRhs(char *fname, void* pvApiCtx)
{
	SciErr sciErr;

	int *piAddressVarTwo = NULL;
	char *strType = NULL;

	sciErr = getVarAddressFromPosition(pvApiCtx, 2, &piAddressVarTwo);
	if(sciErr.iErr)
	{
		printError(&sciErr, 0);
		return NULL;
	}

	if ( !isStringType(pvApiCtx, piAddressVarTwo) )
	{
		Scierror(999, _("%s: Wrong type for input argument #%d: A string expected.\n"), fname, 2);
		return NULL;
	}

	if ( getAllocatedSingleString(pvApiCtx, piAddressVarTwo, &strType) )
	{
		Scierror(999, _("%s: Wrong size for input argument #%d: A string expected.\n"), fname, 2);
		return NULL;
	}

	return strType;
}
/* ==================================================================== */
static char **getLabelStrings(int *sizeReturned)
{
	int i = 0;
	int j = 0;
	char **InfoStrings = NULL;
	*sizeReturned = (int)(SF_STR_LAST - SF_STR_FIRST + 1);
	InfoStrings = (char **)malloc(sizeof(char*) * (*sizeReturned));

	if (InfoStrings == NULL)
	{
		*sizeReturned = 0;
		return NULL;
	}

	j = 0;
	for (i = SF_STR_FIRST; i <= SF_STR_LAST; i++)
	{
		InfoStrings[j] = intTypeToStr(i);
		j++;
	}

	return InfoStrings;
}
/* ==================================================================== */
static char **getValueStrings(char *filename, int *sizeReturned)
{
	int i = 0;
	int j = 0;
	char **values = NULL;
	SNDFILE * file = NULL;
	SF_INFO sfinfo;

	*sizeReturned = (int)(SF_STR_LAST - SF_STR_FIRST + 1);

	values = (char **)malloc(sizeof(char*) * (*sizeReturned));

	if (values == NULL)
	{
		*sizeReturned = 0;
		return NULL;
	}

	memset (&sfinfo, 0, sizeof (sfinfo)) ;
	file = sf_open (filename, SFM_READ, &sfinfo);

	if (file == NULL)
	{
		*sizeReturned = -1;
		return NULL;
	}

	j = 0;
	for (i = SF_STR_FIRST; i <= SF_STR_LAST; i++)
	{
		const char *field = sf_get_string(file, i);
		if (field)
		{
			values[j] = strdup(field);
		}
		else
		{
			values[j] = strdup("");
		}
		j++;
	}

	sf_close(file);

	return values;
}
/* ==================================================================== */
static BOOL isEmptyValueStrings(char **values, int sizeValues)
{
	BOOL bOK = TRUE;

	if (values)
	{
		int i = 0;
		for (i = 0; i < sizeValues; i++)
		{
			if (strcmp(values[i], "") == 0)
			{
				bOK = bOK & TRUE;
			}
			else
			{
				bOK = FALSE;
			}
		}
	}
	return bOK;
}
/* ==================================================================== */
static char **freeStrings(char ** strs, int nbStrs)
{
	if (strs)
	{
		int i = 0;
		for (i = 0; i < nbStrs; i++)
		{
			if (strs[i])
			{
				free(strs[i]);
				strs[i] = NULL;
			}
		}
		strs = NULL;
	}
	return strs;
}
/* ==================================================================== */
static char *getValueString(char *filename, int strType, int *iErrorFile)
{
	SNDFILE * file = NULL;
	SF_INFO sfinfo;

	memset (&sfinfo, 0, sizeof (sfinfo)) ;
	file = sf_open (filename, SFM_READ, &sfinfo);

	if (file)
	{
		const char *dataString = sf_get_string(file, strType);
		char *val = NULL;

		*iErrorFile = 0;

		if (dataString)
		{
			val = strdup(dataString);
			*iErrorFile = 0;
		}
		else
		{
			*iErrorFile = -2;
		}

		sf_close(file);

		return val;
	}

	*iErrorFile = -1;
	return NULL;
}
/* ==================================================================== */

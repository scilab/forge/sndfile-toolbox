// ====================================================================
// Copyright DIGITEO 2010 - Allan CORNET
// ====================================================================
mode(-1);
lines(0);

//setenv("__USE_DEPRECATED_STACK_FUNCTIONS__", "YES");
try
 v = getversion("scilab");
catch
 error(gettext("Scilab 5.3 or more is required."));  
end;
if v(1) < 6 & v(2) < 2 then
 // new API in scilab 5.3
 error(gettext("Scilab 5.3 or more is required."));  
end
clear v;
// ====================================================================
TOOLBOX_NAME  = "sndfile_toolbox";
TOOLBOX_TITLE = "Sndfile Toolbox";
// ====================================================================
toolbox_dir = get_absolute_file_path("builder.sce");

tbx_builder_macros(toolbox_dir);
tbx_builder_gateway(toolbox_dir);
tbx_builder_help(toolbox_dir);
tbx_build_loader(toolbox_dir);
tbx_build_cleaner(toolbox_dir);

clear toolbox_dir TOOLBOX_NAME TOOLBOX_TITLE;
// ====================================================================

// ====================================================================
// Copyright DIGITEO 2010
// Allan CORNET
// This file is released into the public domain
// ====================================================================
lines(0);
r_demopath = get_absolute_file_path("read_ogg_file.dem.sce");
if fileinfo(r_demopath + '/demo.ogg') <> [] then
  disp("[data, samplerate, fmt] = sfread(''" + strsubst(r_demopath,'\','/') + "'' + ''demo.ogg'');");
  [data, samplerate, fmt] = sfread(r_demopath + 'demo.ogg');
  printf("sample rate : %d\n", samplerate);
  printf("format : %s\n", fmt);
  if with_module('sound') then
    scf();
    mapsound(data);
    scf();
    analyze(data);
  else
    disp('datas :');
    disp(data);
  end
end
// ====================================================================
clear r_demopath;
clear data;
clear samplerate;
clear fmt;
// ====================================================================

// ====================================================================
// Copyright DIGITEO - 2010
// Allan CORNET
// This file is released into the public domain
// ====================================================================
lines(0);
w_demopath = get_absolute_file_path("write_snd_file.dem.sce");
if fileinfo(w_demopath + '/wav_16.wav') <> [] then
  printf("[data, samplerate, fmt] = sfread(''" + strsubst(w_demopath,'\','/') + "'' + ''wav_16.wav'');\n");
  [data, samplerate, fmt] = sfread(w_demopath + 'wav_16.wav');
  printf("sample rate : %d\n", samplerate);
  printf("format : %s\n", fmt);
  
  printf("\nsave file in TMPDIR with same format.\n");
  sfwrite(TMPDIR + '/wav_16.wav', data, samplerate, fmt);
  printf("sfwrite(""" + strsubst(TMPDIR,'\','/') + """+ ''/wav_16.wav'' + ,data,samplerate,fmt);\n");
  
  fmts = [ "aiff-int16",    "aiff";
           "aiff-int32",    "aiff";
           "aiff-int8",    "aiff";
           "au-int16",      "au";
           "au-ulaw",       "au";
           "caf-int16",     "caf";
           "flac-int16",    "flac";
           "ogg-vorbis",    "ogg";
           "wav-int16",     "wav";
           "wav-int32",     "wav";
           "wav-ima_adpcm", "wav";
           "wav-ms_adpcm",  "wav"];
  
  for i = 1:size(fmts,'r')
   printf("\nsave file in TMPDIR with %s format.\n", fmts(i,1) );
   sfwrite(TMPDIR + '/file_sound.'+ fmts(i,2), data, samplerate, fmts(i,1));
   printf("sfwrite(""" + strsubst(TMPDIR,'\','/') + "/file_sound"+ string(i) + "." + fmts(i,2) + """, data, samplerate, + """ + fmts(i,1) + """);\n");
  end
  
end
// ====================================================================
clear r_demopath;
clear data;
clear samplerate;
clear fmt;
// ====================================================================

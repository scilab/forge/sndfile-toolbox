// ====================================================================
// Copyright DIGITEO - 2010
// Allan CORNET
// ====================================================================
demopath = get_absolute_file_path("sndfile_toolbox.dem.gateway.sce");

subdemolist = ["read sound file"             ,"read_snd_file.dem.sce"; ..
               "read .ogg file"              ,"read_ogg_file.dem.sce"; ..
               "write sound file"            ,"write_snd_file.dem.sce" ];

subdemolist(:,2) = demopath + subdemolist(:,2);
// ====================================================================

// ====================================================================
// Copyright INRIA 2008
// Allan CORNET
// This file is released into the public domain
// ====================================================================
lines(0);
r_demopath = get_absolute_file_path("read_snd_file.dem.sce");
if fileinfo(r_demopath + '/wav_16.wav') <> [] then
  disp("[data, samplerate, fmt] = sfread(''" + strsubst(r_demopath,'\','/') + "'' + ''wav_16.wav'');");
  [data, samplerate, fmt] = sfread(r_demopath + 'wav_16.wav');
  printf("sample rate : %d\n", samplerate);
  printf("format : %s\n", fmt);
  if with_module('sound') then
    scf();
    mapsound(data);
    scf();
    analyze(data);
  else
    disp('datas :');
    disp(data);
  end
end
// ====================================================================
clear r_demopath;
clear data;
clear samplerate;
clear fmt;
// ====================================================================

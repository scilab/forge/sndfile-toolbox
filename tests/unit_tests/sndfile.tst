// ====================================================================
// Allan CORNET
// DIGITEO 2010
// ====================================================================
// <-- JVM NOT MANDATORY -->
// ====================================================================
tlbxs = atomsGetInstalled();
if grep(tlbxs, 'sndfile_toolbox') <> [] then
  atomsLoad("sndfile_toolbox");
else
  root_path = getenv('TOOLBOX_SNDFILE_PATH','');
  if root_path <> '' then 
    exec(root_path + 'loader.sce'); 
  end 
end
// ====================================================================
filename_dst = pathconvert(TMPDIR + "/saved_flac_16.flac",%F);
ref_samplerate = 44100;
ref_fmt = "flac-int16";
ref_data = [(1:10)*0.1 ; (1:10)*-0.001]';
sfwrite(filename_dst, ref_data, ref_samplerate, ref_fmt);
if ~isfile(filename_dst) then pause, end
[data, samplerate, fmt] = sfread(filename_dst);
if ~and(round(data * 100) == round(ref_data * 100)) then pause, end
// ====================================================================
filename_ref = pathconvert(sfgettlbxpath() + "demos/demo.ogg",%f);
filename_dst = pathconvert(TMPDIR + "/demo.ogg",%F);

[data, samplerate, fmt] = sfread(filename_ref);

if size(data,'*') <> 713152 then pause,end
if samplerate <> 44100 then pause,end
if fmt <> 'ogg-vorbis' then pause,end

sfwrite(filename_dst,data,samplerate,fmt);
if ~isfile(filename_dst) then pause,end
// ====================================================================
filename_ref = pathconvert(sfgettlbxpath() + "tests/unit_tests/wav_16.wav",%f);
filename_dst = pathconvert(TMPDIR + "/created_wav_16.wav",%F);

[data, samplerate, fmt] = sfread(filename_ref);

if size(data,'*') <> 147254 then pause,end
if samplerate <> 44100 then pause,end
if fmt <> 'wav-int16' then pause,end

sfwrite(filename_dst,data,samplerate,fmt);
if ~isfile(filename_dst) then pause,end
// ====================================================================
filename_dst = pathconvert(TMPDIR + "/sfsetgetstring.wav",%F);
ref_samplerate = 44100;
ref_fmt = "wav-int16";
ref_data = [[1:10]*0.1 ; [1:10]*-0.001]';
sfwrite(filename_dst, ref_data, ref_samplerate, ref_fmt);
if ~isfile(filename_dst) then pause, end

REF_TITLE = "sndfile test title";
if ~sfsetstring(filename_dst, "title", REF_TITLE) then pause, end;

REF_COPYRIGHT = "sndfile test copyright";
if ~sfsetstring(filename_dst, "copyright", REF_COPYRIGHT) then pause, end;

REF_SOFTWARE = "sndfile test software";
if ~sfsetstring(filename_dst, "software", REF_SOFTWARE) then pause, end;

REF_ARTIST = "sndfile test artist";
if ~sfsetstring(filename_dst, "artist", REF_ARTIST) then pause, end;

REF_COMMENT = "sndfile test comment";
if ~sfsetstring(filename_dst, "comment", REF_COMMENT) then pause, end;

REF_DATE = "sndfile test date";
if ~sfsetstring(filename_dst, "date", REF_DATE) then pause, end;

REF_ALBUM = "sndfile test album";
if ~sfsetstring(filename_dst, "album", REF_ALBUM) then pause, end;

REF_LICENSE = "sndfile test license";
if ~sfsetstring(filename_dst, "license", REF_LICENSE) then pause, end;

if sfgetstring(filename_dst, "title") <> REF_TITLE then pause, end;
if sfgetstring(filename_dst, "copyright") <> REF_COPYRIGHT then pause, end;
if sfgetstring(filename_dst, "software") <> REF_SOFTWARE + ' (' + sfversion() + ')' then pause, end;
if sfgetstring(filename_dst, "artist") <> REF_ARTIST then pause, end;
if sfgetstring(filename_dst, "comment") <> REF_COMMENT then pause, end;
if sfgetstring(filename_dst, "date") <> REF_DATE then pause, end;
// ====================================================================


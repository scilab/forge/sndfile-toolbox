// ====================================================================
// Allan CORNET
// DIGITEO 2010
// ====================================================================
// <-- JVM NOT MANDATORY -->
// ====================================================================
tlbxs = atomsGetInstalled();
if grep(tlbxs, 'sndfile_toolbox') <> [] then
  atomsLoad("sndfile_toolbox");
else
  root_path = getenv('TOOLBOX_SNDFILE_PATH', '');
  if root_path <> '' then 
    exec(root_path + 'loader.sce'); 
  end 
end
// ====================================================================
cd(TMPDIR);
a = rand(10000,3) * 2 - 1;
sfwrite("test.aiff", a, 20000, "aiff-int32");
[data, samplerate, fmt] = sfread("test.aiff");
if size(data,'*') <> size(a, '*') then pause, end
// ====================================================================

